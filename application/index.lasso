[include:/header.txt]
<SCRIPT LANGUAGE="JavaScript">

<!-- Begin
function numbersonly(field) {
var valid = "0123456789.$"
var ok = "yes";
var temp;
for (var i=0; i<field.value.length; i++) {
temp = "" + field.value.substring(i, i+1);
if (valid.indexOf(temp) == "-1") ok = "no";
}
if (ok == "no") {
alert("Invalid entry!  Only numbers are accepted!");
field.focus();
field.blur();
field.select();
   }
}

function checkName(field) {
  if (field.value == "") {
    alert("Value is required");
	field.focus();
	field.blur();
	field.select();
  }
  else {
    if (field.value.split(" ").length < 2) {
	  alert("Enter a full name");
	  field.focus();
	  field.blur();
	  field.select();
	}
  }
}

function checkEmail(field) {
  if (field.value.indexOf("@") == -1) {
    alert("Enter a valid e-mail address");
	field.focus();
	field.blur();
	field.select();
  }

}
//  End -->
</script>

<form action="page2.lasso" METHOD="POST">
<CENTER><H1>APPLICATION FOR EMPLOYMENT</H1></CENTER>
<hr>
<h2>Personal Information</h2>
Name:<input type="text" name="Name" value="Last, First Middle"><br>
Present Address (+City, State, and Zip):<input type="text" name="PresAddress" value=""><br>
Permanent Address (if different):<input type="text" name="PermAddress" value="same"><br>
Phone number:<input type="text" name="Phone" value=""><br>
E-mail address:<input type="text" name="email" value=""><br>
Social Security Number:<input type="text" name="SSN" onChange="numbersonly(this)" value="xxxxxxxxx"><br>
Are you related to anyone in our employ?<input type="radio" name="Relative" value="Yes">Yes<input type="radio" checked name="Relative" value="No">No<br>
Refered by:<input type="text" name="Referer" value=""><br>

<h2>Employment Desired</h2>
Position:<input type="text" name="Position" value=""><br>
Date you can start:<input type="text" name="startdate" value="mm/dd/yyyy"><br>
Salary Desired:<input type="text" name="SalaryValue" onChange="numbersonly(this)" value=""><select name="SalaryPeriod">
<OPTION VALUE="Hour" SELECTED>per hour</OPTION>
<OPTION VALUE="Week">per week</OPTION>
<OPTION VALUE="Month">per month</OPTION>
<OPTION VALUE="Year">per year</OPTION>
</SELECT><br>
Are you employed now?<input type="radio" checked name="currstatus" value="Yes">Yes<input type="radio" name="currstatus" value="No">No<br>
May we inquire of your present employer?<input type="radio" checked name="currverify" value="Yes">Yes<input type="radio" name="currverify" value="No">No<br>
Have you applied to this company before?<input type="radio" name="prevapplicant" value="Yes">Yes<input type="radio" checked name="prevapplicant" value="No">No<br>

<INPUT TYPE="submit" NAME="Submit" VALUE="Go to page 2">
</form>
<br>
[include:/footer.txt]