[include:/header.txt]
<SCRIPT LANGUAGE="JavaScript">

<!-- Begin
function numbersonly(field) {
var valid = "0123456789.$"
var ok = "yes";
var temp;
for (var i=0; i<field.value.length; i++) {
temp = "" + field.value.substring(i, i+1);
if (valid.indexOf(temp) == "-1") ok = "no";
}
if (ok == "no") {
alert("Invalid entry!  Only numbers are accepted.");
field.focus();
field.select();
   }
}
function checkName(field) {
  if (field.value == "") {
    alert("Value is required");
	field.focus();
	field.blur();
	field.select();
  }
  else {
    if (field.value.split(" ").length < 2) {
	  alert("Enter a full name");
	  field.focus();
	  field.blur();
	  field.select();
	}
  }
}

function checkEmail(field) {
  if (field.value.indexOf("@") == -1) {
    alert("Enter a valid e-mail address");
	field.focus();
	field.blur();
	field.select();
  }

}
//  End -->
</script>
<form action="page3.lasso" METHOD="POST">
<CENTER><H1>APPLICATION FOR EMPLOYMENT PAGE 2</H1></CENTER>
<hr>
<h2>Personal Information</h2>
Name:<input type="HIDDEN" name="Name" value="[form_param:'Name']">[form_param:'Name']<br>
Present Address:<input type="HIDDEN" name="PresAddress" value="[form_param:'PresAddress']">[form_param:'PresAddress']<br>
Permanent Address (if different):<input type="HIDDEN" name="PermAddress" value="[form_param:'PermAddress']">[form_param:'PermAddress']<br>
Phone number:<input type="HIDDEN" name="Phone" value="[form_param:'Phone']">[form_param:'Phone']<br>
E-mail address:<input type="HIDDEN" name="email" value="[form_param:'email']">[form_param:'email']<br>
Social Security Number:<input type="HIDDEN" name="SSN" value="[form_param:'SSN']">[form_param:'SSN']<br>
Are you related to anyone in our employ?<input type="hidden" name="Relative" value="[form_param:'Relative']">[form_param:'Relative']<br>
Refered by:<input type="HIDDEN" name="Referer" value="[form_param:'Referer']">[form_param:'Referer']<br>

<h2>Employment Desired</h2>
Position:<input type="HIDDEN" name="Position" value="[form_param:'Position']">[form_param:'Position']<br>
Date you can start:<input type="HIDDEN" name="startdate" value="[form_param:'startdate']">[form_param:'startdate']<br>
Salary Desired:<input type="HIDDEN" name="SalaryValue" value="[form_param:'SalaryValue']">[form_param:'SalaryValue'] per <input type="HIDDEN" name="SalaryPeriod" value="[form_param:'SalaryPeriod']">[form_param:'SalaryPeriod']<br>
Are you employed now?<input type="HIDDEN" name="currstatus" value="[form_param:'currstatus']">[form_param:'currstatus']<br>
May we inquire of your present employer?<input type="HIDDEN" name="currverify" value="[form_param:'currverify']">[form_param:'currverify']<br>
Have you applied to this company before?<input type="HIDDEN" name="prevapplicant" value="[form_param:'prevapplicant']">[form_param:'prevapplicant']<br>

<h2>Education</h2>
Most Recent First
<table><tr>
<th>Type</th><th>Name and Location</th><th>Graduated</th><th>Major Subject</th><th>GPA</th>
</tr>
<tr>
<td>
<select name="School1_type">
<OPTION VALUE="College" SELECTED>College or University</OPTION>
<OPTION VALUE="Trade">Trade School</OPTION>
<OPTION VALUE="High School">High School</OPTION>
<OPTION VALUE="Grammar">Grammar School</OPTION>
<OPTION VALUE="Other">Other School</OPTION>
</SELECT>
</td>
<td><input type="text" name="school1_name" value="school name"><br><input type="text" name="school1_loc" value="school location">
</td>
<td>
<input type="radio" checked name="school1_grad" value="Yes">Yes<br><input type="radio" name="school1_grad" value="No">No
</td>
<td>
<input type="text" name="school1_major" value="">
</td>
<td>
<select name="School1_gpa">
<OPTION VALUE="4.0" SELECTED>4.0</OPTION>
<OPTION VALUE="3.9">3.9</OPTION>
<OPTION VALUE="3.8">3.8</OPTION>
<OPTION VALUE="3.7">3.7</OPTION>
<OPTION VALUE="3.6">3.6</OPTION>
<OPTION VALUE="3.5">3.5</OPTION>
<OPTION VALUE="3.4">3.4</OPTION>
<OPTION VALUE="3.3">3.3</OPTION>
<OPTION VALUE="3.2">3.2</OPTION>
<OPTION VALUE="3.1">3.1</OPTION>
<OPTION VALUE="3.0">3.0</OPTION>
<OPTION VALUE="2.9">2.9</OPTION>
<OPTION VALUE="2.8">2.8</OPTION>
<OPTION VALUE="2.7">2.7</OPTION>
<OPTION VALUE="2.6">2.6</OPTION>
<OPTION VALUE="2.5">2.5</OPTION>
<OPTION VALUE="2.4">2.4</OPTION>
<OPTION VALUE="2.3">2.3</OPTION>
<OPTION VALUE="2.2">2.2</OPTION>
<OPTION VALUE="2.1">2.1</OPTION>
<OPTION VALUE="2.0">2.0</OPTION>
<OPTION VALUE="1.9">1.9</OPTION>
<OPTION VALUE="1.8">1.8</OPTION>
<OPTION VALUE="1.7">1.7</OPTION>
<OPTION VALUE="1.6">1.6</OPTION>
<OPTION VALUE="1.5">1.5</OPTION>
<OPTION VALUE="1.4">1.4</OPTION>
<OPTION VALUE="1.3">1.3</OPTION>
<OPTION VALUE="1.2">1.2</OPTION>
<OPTION VALUE="1.1">1.1</OPTION>
<OPTION VALUE="1.0">1.0</OPTION>
<OPTION VALUE="0.9">0.9</OPTION>
<OPTION VALUE="0.8">0.8</OPTION>
<OPTION VALUE="0.7">0.7</OPTION>
<OPTION VALUE="0.6">0.6</OPTION>
<OPTION VALUE="0.5">0.5</OPTION>
<OPTION VALUE="0.4">0.4</OPTION>
<OPTION VALUE="0.3">0.3</OPTION>
<OPTION VALUE="0.2">0.2</OPTION>
<OPTION VALUE="0.1">0.1</OPTION>
<OPTION VALUE="0.0">0.0</OPTION>
</SELECT>
</td>
</tr>

<tr>
<td>
<select name="School2_type">
<OPTION VALUE="N/A" SELECTED>N/A</OPTION>
<OPTION VALUE="College">College or University</OPTION>
<OPTION VALUE="Trade">Trade School</OPTION>
<OPTION VALUE="High School">High School</OPTION>
<OPTION VALUE="Grammar">Grammar School</OPTION>
<OPTION VALUE="Other">Other School</OPTION>
</SELECT>
</td>
<td><input type="text" name="school2_name" value=""><br><input type="text" name="school2_loc" value="">
</td>
<td>
<input type="radio" checked name="school2_grad" value="Yes">Yes<br><input type="radio" name="school2_grad" value="No">No
</td>
<td>
<input type="text" name="school2_major" value="">
</td>
<td>
<select name="School2_gpa">
<OPTION VALUE="4.0" SELECTED>4.0</OPTION>
<OPTION VALUE="3.9">3.9</OPTION>
<OPTION VALUE="3.8">3.8</OPTION>
<OPTION VALUE="3.7">3.7</OPTION>
<OPTION VALUE="3.6">3.6</OPTION>
<OPTION VALUE="3.5">3.5</OPTION>
<OPTION VALUE="3.4">3.4</OPTION>
<OPTION VALUE="3.3">3.3</OPTION>
<OPTION VALUE="3.2">3.2</OPTION>
<OPTION VALUE="3.1">3.1</OPTION>
<OPTION VALUE="3.0">3.0</OPTION>
<OPTION VALUE="2.9">2.9</OPTION>
<OPTION VALUE="2.8">2.8</OPTION>
<OPTION VALUE="2.7">2.7</OPTION>
<OPTION VALUE="2.6">2.6</OPTION>
<OPTION VALUE="2.5">2.5</OPTION>
<OPTION VALUE="2.4">2.4</OPTION>
<OPTION VALUE="2.3">2.3</OPTION>
<OPTION VALUE="2.2">2.2</OPTION>
<OPTION VALUE="2.1">2.1</OPTION>
<OPTION VALUE="2.0">2.0</OPTION>
<OPTION VALUE="1.9">1.9</OPTION>
<OPTION VALUE="1.8">1.8</OPTION>
<OPTION VALUE="1.7">1.7</OPTION>
<OPTION VALUE="1.6">1.6</OPTION>
<OPTION VALUE="1.5">1.5</OPTION>
<OPTION VALUE="1.4">1.4</OPTION>
<OPTION VALUE="1.3">1.3</OPTION>
<OPTION VALUE="1.2">1.2</OPTION>
<OPTION VALUE="1.1">1.1</OPTION>
<OPTION VALUE="1.0">1.0</OPTION>
<OPTION VALUE="0.9">0.9</OPTION>
<OPTION VALUE="0.8">0.8</OPTION>
<OPTION VALUE="0.7">0.7</OPTION>
<OPTION VALUE="0.6">0.6</OPTION>
<OPTION VALUE="0.5">0.5</OPTION>
<OPTION VALUE="0.4">0.4</OPTION>
<OPTION VALUE="0.3">0.3</OPTION>
<OPTION VALUE="0.2">0.2</OPTION>
<OPTION VALUE="0.1">0.1</OPTION>
<OPTION VALUE="0.0">0.0</OPTION>
</SELECT>
</td>
</tr>

<tr>
<td>
<select name="School3_type">
<OPTION VALUE="N/A" SELECTED>N/A</OPTION>
<OPTION VALUE="College">College or University</OPTION>
<OPTION VALUE="Trade">Trade School</OPTION>
<OPTION VALUE="High School">High School</OPTION>
<OPTION VALUE="Grammar">Grammar School</OPTION>
<OPTION VALUE="Other">Other School</OPTION>
</SELECT>
</td>
<td><input type="text" name="school3_name" value=""><br><input type="text" name="school3_loc" value="">
</td>
<td>
<input type="radio" checked name="school3_grad" value="Yes">Yes<br><input type="radio" name="school3_grad" value="No">No
</td>
<td>
<input type="text" name="school3_major" value="">
</td>
<td>
<select name="School3_gpa">
<OPTION VALUE="4.0" SELECTED>4.0</OPTION>
<OPTION VALUE="3.9">3.9</OPTION>
<OPTION VALUE="3.8">3.8</OPTION>
<OPTION VALUE="3.7">3.7</OPTION>
<OPTION VALUE="3.6">3.6</OPTION>
<OPTION VALUE="3.5">3.5</OPTION>
<OPTION VALUE="3.4">3.4</OPTION>
<OPTION VALUE="3.3">3.3</OPTION>
<OPTION VALUE="3.2">3.2</OPTION>
<OPTION VALUE="3.1">3.1</OPTION>
<OPTION VALUE="3.0">3.0</OPTION>
<OPTION VALUE="2.9">2.9</OPTION>
<OPTION VALUE="2.8">2.8</OPTION>
<OPTION VALUE="2.7">2.7</OPTION>
<OPTION VALUE="2.6">2.6</OPTION>
<OPTION VALUE="2.5">2.5</OPTION>
<OPTION VALUE="2.4">2.4</OPTION>
<OPTION VALUE="2.3">2.3</OPTION>
<OPTION VALUE="2.2">2.2</OPTION>
<OPTION VALUE="2.1">2.1</OPTION>
<OPTION VALUE="2.0">2.0</OPTION>
<OPTION VALUE="1.9">1.9</OPTION>
<OPTION VALUE="1.8">1.8</OPTION>
<OPTION VALUE="1.7">1.7</OPTION>
<OPTION VALUE="1.6">1.6</OPTION>
<OPTION VALUE="1.5">1.5</OPTION>
<OPTION VALUE="1.4">1.4</OPTION>
<OPTION VALUE="1.3">1.3</OPTION>
<OPTION VALUE="1.2">1.2</OPTION>
<OPTION VALUE="1.1">1.1</OPTION>
<OPTION VALUE="1.0">1.0</OPTION>
<OPTION VALUE="0.9">0.9</OPTION>
<OPTION VALUE="0.8">0.8</OPTION>
<OPTION VALUE="0.7">0.7</OPTION>
<OPTION VALUE="0.6">0.6</OPTION>
<OPTION VALUE="0.5">0.5</OPTION>
<OPTION VALUE="0.4">0.4</OPTION>
<OPTION VALUE="0.3">0.3</OPTION>
<OPTION VALUE="0.2">0.2</OPTION>
<OPTION VALUE="0.1">0.1</OPTION>
<OPTION VALUE="0.0">0.0</OPTION>
</SELECT>
</td>
</tr>

<tr>
<td>
<select name="School4_type">
<OPTION VALUE="N/A" SELECTED>N/A</OPTION>
<OPTION VALUE="College">College or University</OPTION>
<OPTION VALUE="Trade">Trade School</OPTION>
<OPTION VALUE="High School">High School</OPTION>
<OPTION VALUE="Grammar">Grammar School</OPTION>
<OPTION VALUE="Other">Other School</OPTION>
</SELECT>
</td>
<td><input type="text" name="school4_name" value=""><br><input type="text" name="school4_loc" value="">
</td>
<td>
<input type="radio" checked name="school4_grad" value="Yes">Yes<br><input type="radio" name="school4_grad" value="No">No
</td>
<td>
<input type="text" name="school4_major" value="">
</td>
<td>
<select name="School4_gpa">
<OPTION VALUE="4.0" SELECTED>4.0</OPTION>
<OPTION VALUE="3.9">3.9</OPTION>
<OPTION VALUE="3.8">3.8</OPTION>
<OPTION VALUE="3.7">3.7</OPTION>
<OPTION VALUE="3.6">3.6</OPTION>
<OPTION VALUE="3.5">3.5</OPTION>
<OPTION VALUE="3.4">3.4</OPTION>
<OPTION VALUE="3.3">3.3</OPTION>
<OPTION VALUE="3.2">3.2</OPTION>
<OPTION VALUE="3.1">3.1</OPTION>
<OPTION VALUE="3.0">3.0</OPTION>
<OPTION VALUE="2.9">2.9</OPTION>
<OPTION VALUE="2.8">2.8</OPTION>
<OPTION VALUE="2.7">2.7</OPTION>
<OPTION VALUE="2.6">2.6</OPTION>
<OPTION VALUE="2.5">2.5</OPTION>
<OPTION VALUE="2.4">2.4</OPTION>
<OPTION VALUE="2.3">2.3</OPTION>
<OPTION VALUE="2.2">2.2</OPTION>
<OPTION VALUE="2.1">2.1</OPTION>
<OPTION VALUE="2.0">2.0</OPTION>
<OPTION VALUE="1.9">1.9</OPTION>
<OPTION VALUE="1.8">1.8</OPTION>
<OPTION VALUE="1.7">1.7</OPTION>
<OPTION VALUE="1.6">1.6</OPTION>
<OPTION VALUE="1.5">1.5</OPTION>
<OPTION VALUE="1.4">1.4</OPTION>
<OPTION VALUE="1.3">1.3</OPTION>
<OPTION VALUE="1.2">1.2</OPTION>
<OPTION VALUE="1.1">1.1</OPTION>
<OPTION VALUE="1.0">1.0</OPTION>
<OPTION VALUE="0.9">0.9</OPTION>
<OPTION VALUE="0.8">0.8</OPTION>
<OPTION VALUE="0.7">0.7</OPTION>
<OPTION VALUE="0.6">0.6</OPTION>
<OPTION VALUE="0.5">0.5</OPTION>
<OPTION VALUE="0.4">0.4</OPTION>
<OPTION VALUE="0.3">0.3</OPTION>
<OPTION VALUE="0.2">0.2</OPTION>
<OPTION VALUE="0.1">0.1</OPTION>
<OPTION VALUE="0.0">0.0</OPTION>
</SELECT>
</td>
</tr>
</table>

Special Study or Research Work:<INPUT TYPE="TEXT" NAME="specstudies"><br>
Activities (civic, athletic, etc.):<INPUT TYPE="TEXT" NAME="activities"><br>

<INPUT TYPE="submit" NAME="Submit" VALUE="Go to page 3">
</form>
<br>
[include:/footer.txt]