[include:/header.txt]
<SCRIPT LANGUAGE="JavaScript">

<!-- Begin
function numbersonly(field) {
var valid = "0123456789.$"
var ok = "yes";
var temp;
for (var i=0; i<field.value.length; i++) {
temp = "" + field.value.substring(i, i+1);
if (valid.indexOf(temp) == "-1") ok = "no";
}
if (ok == "no") {
alert("Invalid entry!  Only numbers are accepted!");
field.focus();
field.select();
   }
}
function checkName(field) {
  if (field.value == "") {
    alert("Value is required");
	field.focus();
	field.blur();
	field.select();
  }
  else {
    if (field.value.split(" ").length < 2) {
	  alert("Enter a full name");
	  field.focus();
	  field.blur();
	  field.select();
	}
  }
}

function checkEmail(field) {
  if (field.value.indexOf("@") == -1) {
    alert("Enter a valid e-mail address");
	field.focus();
	field.blur();
	field.select();
  }

}
//  End -->
</script>

<form action="page4.lasso" METHOD="POST">
<CENTER><H1>APPLICATION FOR EMPLOYMENT PAGE 3</H1></CENTER>
<hr>
<h2>Personal Information</h2>
Name:<input type="HIDDEN" name="Name" value="[form_param:'Name']">[form_param:'Name']<br>
<input type="HIDDEN" name="PresAddress" value="[form_param:'PresAddress']">
<input type="HIDDEN" name="PermAddress" value="[form_param:'PermAddress']">
<input type="HIDDEN" name="Phone" value="[form_param:'Phone']">
<input type="HIDDEN" name="email" value="[form_param:'email']">
<input type="HIDDEN" name="SSN" value="[form_param:'SSN']">
<input type="hidden" name="Relative" value="[form_param:'Relative']">
<input type="HIDDEN" name="Referer" value="[form_param:'Referer']">

<h2>Employment Desired</h2>
Position:<input type="HIDDEN" name="Position" value="[form_param:'Position']">[form_param:'Position']<br>
<input type="HIDDEN" name="startdate" value="[form_param:'startdate']">
<input type="HIDDEN" name="SalaryValue" value="[form_param:'SalaryValue']">
<input type="HIDDEN" name="SalaryPeriod" value="[form_param:'SalaryPeriod']">
<input type="HIDDEN" name="currstatus" value="[form_param:'currstatus']">
<input type="HIDDEN" name="currverify" value="[form_param:'currverify']">
<input type="HIDDEN" name="prevapplicant" value="[form_param:'prevapplicant']">

<h2>Education</h2>

<table><tr>
<th>Type</th><th>Name and Location</th><th>Graduated</th><th>Major Subject</th><th>GPA</th>
</tr>
<tr>
<td>
<input type="HIDDEN" name="school1_type" value="[form_param:'school1_type']">[form_param:'school1_type']
</td>
<td><input type="HIDDEN" name="school1_name" value="[form_param:'school1_name']">[form_param:'school1_name']<br><input type="HIDDEN" name="school1_loc" value="[form_param:'school1_loc']">[form_param:'school1_loc']
</td>
<td>
<input type="HIDDEN" name="school1_grad" value="[form_param:'school1_grad']">[form_param:'school1_grad']
</td>
<td>
<input type="HIDDEN" name="school1_major" value="[form_param:'school1_major']">
</td>
<td>
<input type="HIDDEN" name="School1_gpa" value="[form_param:'school1_gpa']">[form_param:'school1_gpa']
</td>
</tr>

<tr>
<td>
<input type="HIDDEN" name="school2_type" value="[form_param:'school2_type']">[form_param:'school2_type']
</td>
<td><input type="HIDDEN" name="school2_name" value="[form_param:'school2_name']">[form_param:'school2_name']<br><input type="HIDDEN" name="school2_loc" value="[form_param:'school2_loc']">[form_param:'school2_loc']
</td>
<td>
<input type="HIDDEN" name="school2_grad" value="[form_param:'school2_grad']">[form_param:'school2_grad']
</td>
<td>
<input type="HIDDEN" name="school2_major" value="[form_param:'school2_major']">
</td>
<td>
<input type="HIDDEN" name="School2_gpa" value="[form_param:'school2_gpa']">[form_param:'school2_gpa']
</td>
</tr>

<tr>
<td>
<input type="HIDDEN" name="school3_type" value="[form_param:'school3_type']">[form_param:'school3_type']
</td>
<td><input type="HIDDEN" name="school3_name" value="[form_param:'school3_name']">[form_param:'school3_name']<br><input type="HIDDEN" name="school3_loc" value="[form_param:'school3_loc']">[form_param:'school3_loc']
</td>
<td>
<input type="HIDDEN" name="school3_grad" value="[form_param:'school3_grad']">[form_param:'school3_grad']
</td>
<td>
<input type="HIDDEN" name="school3_major" value="[form_param:'school3_major']">
</td>
<td>
<input type="HIDDEN" name="School3_gpa" value="[form_param:'school3_gpa']">[form_param:'school3_gpa']
</td>
</tr>

<tr>
<td>
<input type="HIDDEN" name="school4_type" value="[form_param:'school4_type']">[form_param:'school4_type']
</td>
<td><input type="HIDDEN" name="school4_name" value="[form_param:'school4_name']">[form_param:'school4_name']<br><input type="HIDDEN" name="school4_loc" value="[form_param:'school4_loc']">[form_param:'school4_loc']
</td>
<td>
<input type="HIDDEN" name="school4_grad" value="[form_param:'school4_grad']">[form_param:'school4_grad']
</td>
<td>
<input type="HIDDEN" name="school4_major" value="[form_param:'school4_major']">[form_param:'school4_major']
</td>
<td>
<input type="HIDDEN" name="School4_gpa" value="[form_param:'school4_gpa']">[form_param:'school4_gpa']
</td>
</tr>
</table>

Special Study or Research Work:<INPUT TYPE="HIDDEN" NAME="specstudies" VALUE="[form_param:'specstudies']">[form_param:'specstudies']<br>
Activities (civic, athletic, etc.):<INPUT TYPE="HIDDEN" NAME="activities" VALUE="[form_param:'activities']">[form_param:'activities']<br>


<h2>Previous Employment</h2>
List last four employers beginning with present or most recent

<TABLE>
<tr><th>From</th><th>To:</th><th>Name and Address of Employer</th><th>Monthly Salary</th><th>Position</th><th>Reason for leaving</th></tr>

<tr><td><input type="text" name="job1_from"></td><td><input type="text" name="job1_to"></td><td><input type="text" name="job1_company"><br><input type="text" name="job1_address"><br><input type="text" name="job1_address2"></td><td><input type="text" onChange="numbersonly(this)" name="job1_salary"></td><td><input type="text" name="job1_position"></td><td><input type="text" name="job1_sepreason"></td></tr>

<tr><td><input type="text" name="job2_from"></td><td><input type="text" name="job2_to"></td><td><input type="text" name="job2_company"><br><input type="text" name="job2_address"><br><input type="text" name="job2_address2"></td><td><input type="text" onChange="numbersonly(this)" name="job2_salary"></td><td><input type="text" name="job2_position"></td><td><input type="text" name="job2_sepreason"></td></tr>

<tr><td><input type="text" name="job3_from"></td><td><input type="text" name="job3_to"></td><td><input type="text" name="job3_company"><br><input type="text" name="job3_address"><br><input type="text" name="job3_address2"></td><td><input type="text" onChange="numbersonly(this)" name="job3_salary"></td><td><input type="text" name="job3_position"></td><td><input type="text" name="job3_sepreason"></td></tr>

<tr><td><input type="text" name="job4_from"></td><td><input type="text" name="job4_to"></td><td><input type="text" name="job4_company"><br><input type="text" name="job4_address"><br><input type="text" name="job4_address2"></td><td><input type="text" onChange="numbersonly(this)" name="job4_salary"></td><td><input type="text" name="job4_position"></td><td><input type="text" name="job4_sepreason"></td></tr>

</table>

<h2>References</h2>
Give the names of three persons not related to you whom you have known for at least one year.
<TABLE>
<tr><th>Name</th><th>Address</th><th>Phone</th><th>Business</th><th>Years aquainted</th></tr>
<tr><td><input type="text" name="ref1_name"></td><td><input type="text" name="ref1_address"><br><input type="text" name="ref1_address2"></td><td><input type="text" name="ref1_phone"></td><td><input type="text" name="ref1_business"></td><td><input type="text" onChange="numbersonly(this)" name="ref1_years"></td></tr>
<tr><td><input type="text" name="ref2_name"></td><td><input type="text" name="ref2_address"><br><input type="text" name="ref2_address2"></td><td><input type="text" name="ref2_phone"></td><td><input type="text" name="ref2_business"></td><td><input type="text" onChange="numbersonly(this)" name="ref2_years"></td></tr>
<tr><td><input type="text" name="ref3_name"></td><td><input type="text" name="ref3_address"><br><input type="text" name="ref3_address2"></td><td><input type="text" name="ref3_phone"></td><td><input type="text" name="ref3_business"></td><td><input type="text" onChange="numbersonly(this)" name="ref3_years"></td></tr>

</TABLE>

<hr>
By submitting this application, I authorize investigation of all statements contained in this application.  I understand that misrepresentation or ommision of facts called for is cause for dismissal.  Further, I understand and agree that my employment is for no definite period and may, regardless of the date of payment of my wages and salary, be terminated at any time without any previous notice.

<INPUT TYPE="submit" NAME="Submit" VALUE="Submit Appication">
</form>
<br>
[include:/footer.txt]