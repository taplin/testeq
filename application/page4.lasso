[include:/header.txt]

[inline: Email.Host='mailhost.rf.net', Email.From='jfisher2@testeq.com', Email.To='jfisher2@testeq.com', Email.Subject='Employment Application', Email.Format='/application/application.txt', Nothing, ReUseFormParams][/inline]

Thank you for applying.  After we have the opportunity to review your application we may want to schedule an interview.<p>

If you would like to submit a resume you can e-mail it to me as straight text or a PDF at jfisher2@testeq.com.<p>

John E. Fisher, II<br>
General Manager


[include:/footer.txt]